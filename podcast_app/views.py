from typing import Any
from django.db.models.query import QuerySet
from django.views.generic import ListView, DetailView

from .models import Release, NewsBlog


class AllPodcastView(ListView):
    template_name = "homepage.html"
    model = Release
    queryset = Release.objects.filter().order_by("-pub_date")[:20]
    
class EveryPodcastView(ListView):
    template_name = "homepage.html"
    model = Release

    def get_queryset(self):
        podcast_name = self.request.GET.get('podcast_name')
        if podcast_name == None:
            queryset = []
        else:
            queryset = Release.objects.filter(podcast_name=podcast_name).order_by("-pub_date")[:10]
        return queryset
    
class NewsBlogList(ListView):
    model = NewsBlog
    queryset = NewsBlog.objects.filter().order_by("-pub_date")[:10]
    template_name = "newsblog_list.html"

class EveryNewsBlogView(NewsBlogList):
    template_name = "newsblog_list.html"

    def get_queryset(self):
        section = self.request.GET.get("section")
        if section == None:
            queryset = []
        else:
            queryset = NewsBlog.objects.filter(section=section).order_by("-pub_date")[:10]
            return queryset