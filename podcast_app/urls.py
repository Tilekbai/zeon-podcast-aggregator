from django.urls import path, include

from .views import *

urlpatterns = [
    path("", include("users.urls")),
    path("", AllPodcastView.as_view(), name="homepage"),
    path("podcast", EveryPodcastView.as_view(), name="every-podcast-list"),
    path("newsblog", NewsBlogList.as_view(), name="news-blog-list"),
    path("news", EveryNewsBlogView.as_view(), name="every-news-blog"),
]